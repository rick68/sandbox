{ hello ? { outPath = ./.; revCount = 1234; shortRev = "abcedf"; }
, officialRelease ? false
}:

let

  pkgs = import <nixpkgs> {};

  systems = [ "i686-linux" "x86_64-linux" ];


  jobs = rec {


    tarball =
      with pkgs;

      releaseTools.sourceTarball {
        name = "hello-tarball";
        version = builtins.readFile ./version;
        versionSuffix = if officialRelease then "" else "pre${toString hello.revCount}_${hello.shortRev}";
        src = if lib.inNixShell then null else hello;
        inherit officialRelease;

        buildInputs =
          [ pkgconfig check
          ] ++ lib.optional (!lib.inNixShell) git;

        configureFlags = "";

        postUnpack = ''
          # Clean up when building from a working tree.
          if [[ -d $sourceRoot/.git ]]; then
              git -C $sourceRoot clean -fd
          fi
        '';

        preConfigure = "";

        distPhase =
          ''
            runHook preDist
            make dist
            mkdir -p $out/tarballs
            cp *.tar.* $out/tarballs
          '';

        preDist = ''
          make install
        '';
      };


    build = pkgs.lib.genAttrs systems (system:

      # FIXME: temporarily use a different branch for the Darwin build.
      with import (if system == "x86_64-darwin" then <nixpkgs-darwin> else <nixpkgs>) { inherit system; };

      releaseTools.nixBuild {
        name = "hello";
        src = tarball;

        buildInputs =
          [ pkgconfig check ];

        configureFlags = "";

        enableParallelBuilding = true;

        makeFlags = "profiledir=$(out)/etc/profile.d";

        preBuild = "unset NIX_INDENT_MAKE";

        installFlags = "sysconfdir=$(out)/etc";

        doInstallCheck = true;
        installCheckFlags = "sysconfdir=$(out)/etc";
      }

    );


    binaryTarball = pkgs.lib.genAttrs systems (system:

      # FIXME: temporarily use a different branch for the Darwin build.
      with import (if system == "x86_64-darwin" then <nixpkgs-darwin> else <nixpkgs>) { inherit system; };

      let
        toplevel = builtins.getAttr system jobs.build;
        version = toplevel.src.version;
      in

      releaseTools.binaryTarball {
        name = "hello-binary-tarball";
        src = tarball;
        buildInputs = [ pkgconfig check ];
        prefix = "/usr";
      }

    );


    coverage =
      with import <nixpkgs> { system = "x86_64-linux"; };

      releaseTools.coverageAnalysis {
        name = "hello-coverage";
        src = tarball;

        buildInputs =
          [ pkgconfig check
            # These are for "make check" only
            graphviz libxml2 libxslt
            # This generates Cobertura-style XML reports
            pypyPackages.gcovr
          ];

        configureFlags = "--enable-code-coverage";

        dontInstall = false;

        doInstallCheck = true;

        lcovFilter = []; # [ "/boost/*" "*-tab.*" ];
      };


    # System tests.
    tests.binaryTarball =
      with import <nixpkgs> { system = "x86_64-linux"; };
      vmTools.runInLinuxImage (runCommand "hello-binary-tarball-test"
        { diskImage = vmTools.diskImages.ubuntu1204x86_64;
        }
        ''
          cd /
          tar xf ${binaryTarball.x86_64-linux}/tarballs/*.tar.*
          hello
          mkdir -p $out/nix-support
          touch $out/nix-support/hydra-build-products
        ''
      );


    # Aggregate job containing the release-critical jobs.
    release = pkgs.releaseTools.aggregate {
      name = "hello-${tarball.version}";
      meta.description = "Release-critical builds";
      constituents =
        [ tarball
          build.i686-linux
          build.x86_64-linux
          binaryTarball.i686-linux
          binaryTarball.x86_64-linux
          tests.binaryTarball
        ];


    };


  };


in jobs
