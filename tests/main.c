#include <stdio.h>
#include <stdlib.h>

#include <check.h>

START_TEST(test_hello)
{
}
END_TEST

Suite * hello_suite(void)
{
    Suite* s;
    TCase* tc_core;

    s = suite_create("Hello");

    /* Core test case */
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_hello);
    suite_add_tcase(s, tc_core);

    return s;
}

int main(void)
{
    int number_failed = 0;
    Suite *s;
    SRunner *sr;

    s = hello_suite();
    sr = srunner_create(s);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
