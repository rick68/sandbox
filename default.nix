{ pkgs ? import <nixpkgs> {} }:

with pkgs;

stdenv.mkDerivation {
  name = "hello";
  src = ./.;
  buildInputs = [
    autoconf automake gnumake gawk clang
    pkgconfig check
  ];
  preConfigurePhases = ./bootstrap;
}
