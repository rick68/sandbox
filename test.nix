{ system ? builtins.currentSystem
, pkgs ? import <nixpkgs> { inherit system; }
}:

with pkgs;

stdenv.mkDerivation rec {
  name = "hello-test";
  src = ./.;
  buildInputs = [
    autoconf automake gnumake gawk clang lcov pypyPackages.gcovr
    pkgconfig check
  ];
  preConfigurePhases = ./bootstrap;
  configureFlags = [ "--enable-code-coverage" ];
  doCheck = true;
  postCheck = ''
    make gcovr-report
    mkdir -p ''${out}/share/doc/hello
    cp -a gcovr ''${out}/share/doc/hello
  '';
  dontStrip = true;
}
